<?php
include 'includes/haut.php';

if(isset($_GET['music'])){
	$array_query_musics = array($_GET['music']);
	$where_query_musics = 'WHERE musiques_id=?';
}else if(isset($_GET['artiste'])){
	$array_query_musics = array($_GET['artiste']);
	$where_query_musics = 'WHERE artistes_id=?';
}else{
	$array_query_musics = array();
	$where_query_musics = '';
}

$query_musics = $bdd->prepare('SELECT * FROM musiques INNER JOIN artistes ON musiques_artiste=artistes_id '.$where_query_musics) or die(mysql_error());
$query_musics->execute($array_query_musics);
for($nb_musics_found=0;$fetch_musics = $query_musics->fetch();$nb_musics_found++){
	$musicId = $fetch_musics['musiques_id'];
	?>
	
	<div class="music">		
		<audio class="audioPlayer" id="audioPlayer_<?php echo $musicId; ?>" ontimeupdate="update(<?php echo $musicId; ?>);" >
			<?php			
			$query_ext = $bdd->query('SELECT * FROM extensions ORDER BY extensions_id DESC') or die(mysql_error());
			for($i=0;$fetch_ext = $query_ext->fetch();$i++){
				$musicPath[$fetch_ext['extensions_nom']] = 'musics/'.$fetch_musics['artistes_id'].'/'.$musicId.'.'.$fetch_ext['extensions_nom'];
				if(file_exists($musicPath[$fetch_ext['extensions_nom']]))
					echo '<source src="'.$musicPath[$fetch_ext['extensions_nom']].'" type="audio/'.$fetch_ext['extensions_type'].'" >';
				else
					unset($musicPath[$fetch_ext['extensions_nom']]);
			}			
			?>
			Votre navigateur n'est pas compatible avec le lecteur audio. Merci de t�l�charger un navigateur plus r�cent.
		</audio>
		
		<?php
		if($i==0)
			echo 'Pas de piste audio upload�e';
		?>
				
		<div>			
			<div class="play" id="play_<?php echo $musicId; ?>" ></div>
			<div class="musicInfos">
				<div class="artisteName" ><?php echo $fetch_musics['artistes_nom']; ?></div>
				<div class="musicName" ><?php echo $fetch_musics['musiques_nom']; ?></div>
			</div>
			<div class="clr"></div>
		</div>
		
		<div class="audioBar barGroup" id="barGroup_<?php echo $musicId; ?>">
			<div class="bufferedBar audioBar" id="bufferedBar_<?php echo $musicId; ?>"></div>
			<div class="progressBar audioBar" id="progressBar_<?php echo $musicId; ?>"></div>
			<div class="waveBar audioBar"></div>
		</div>
		
		<div class="controlBar">
			<div class="control" id="audioState_<?php echo $musicId; ?>"></div>
			<div class="downloadDiv">
				T�l�charger en 
				<?php
				foreach($musicPath as $ext=>$music)
					echo '<a class="downloadLink" href="'.$music.'">'.$ext.'</a>';
				?>
			</div>
		</div>
	</div>
	<?php
}

if($nb_musics_found==0)
	echo 'Pas de r�sultat trouv�. <a href="musics.php">Retour</a>';

include 'includes/bas.php';
?>