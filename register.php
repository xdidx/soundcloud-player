<?php
include 'includes/init.php';

if(isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['mdp']) && isset($_POST['confirm_mdp']) && isset($_POST['dateNais_d']) && isset($_POST['dateNais_m']) && isset($_POST['dateNais_y'])){
		
	/*VERIFICATION SI DEJA EXISTANT DANS BDD*/
	if(trim($_POST['nom'])=='')
		$nomIncomplet=true;
	if(trim($_POST['email'])=='')
		$emailIncomplet=true;
	if(trim($_POST['mdp'])=='')
		$mdpIncomplet=true;
	if(trim($_POST['confirm_mdp'])=='')
		$confirmIncomplet=true;		
	
	if(!isset($nomIncomplet) && !isset($emailIncomplet) && !isset($mdpIncomplet) && !isset($confirmIncomplet)){
	
		if(($_POST['dateNais_d']>=1 && $_POST['dateNais_d']<=31)
		&& ($_POST['dateNais_m']>=1 && $_POST['dateNais_m']<=12)
		&& ($_POST['dateNais_y']>=(date('Y')-100) && $_POST['dateNais_y']<=date('Y')))
			$dateNais=$_POST['dateNais_y'].'-'.$_POST['dateNais_m'].'-'.$_POST['dateNais_d'];
		else
			$dateNais=date('Y').'-01-01';
			
		if(!preg_match("^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,4}$", $_POST['email']))
			$emailInvalide=true;
		else if(strlen($_POST['mdp'])<6)
			$mdpTpCourt=true;
		else if($_POST['mdp']!=$_POST['confirm_mdp'])
			$mdpDiff=true;
		else{
			$query_inscription = $bdd->prepare('SELECT * FROM artistes WHERE artistes_email=?') or die(mysql_error());
			$query_inscription->execute(array($_POST['email']));
			if($fetch_inscription = $query_inscription->fetch())
				$emailUtilise=true;
			else{
				$query_inscription = $bdd->prepare('SELECT * FROM artistes WHERE artistes_nom=?') or die(mysql_error());
				$query_inscription->execute(array($_POST['nom']));
				if($fetch_inscription = $query_inscription->fetch())
					$nomUtilise=true;
				else{
					$query_inscription = $bdd->prepare('INSERT INTO artistes(artistes_nom, artistes_email, artistes_mdp, artistes_dateNais) values(?,?,?,?)') or die(mysql_error());
					$query_inscription->execute(array($_POST['nom'],$_POST['email'],md5($_POST['mdp']),$dateNais));
					header('location:connexion.php?inscri');
				}
			}
		}
	}
}

include 'includes/haut.php';
?>

<div class="alert">
	<?php
	if(isset($emailInvalide))
		echo 'L\'email saisie est invalide.';
	else if(isset($mdpTpCourt))
		echo 'Le mot de passe est trop court (minimum 6 caract�res).';
	else if(isset($mdpDiff))
		echo 'Le mot de passe et la confirmation sont diff�rents.';
	else if(isset($emailUtilise))
		echo 'L\'email saisie est d�j� utilis�e.';
	else if(isset($nomUtilise))
		echo 'Le nom saisie est d�j� utilis�.';
	?>
</div>

<form action="register.php" method="post">

	<fieldset class="forms">
		<legend>Informations du compte</legend>
		<div>
			<label <?php if(isset($nomIncomplet)) echo 'class="alert"'; ?> for="nom">Nom</label>
			<input type="text" name="nom" id="nom" <?php if(isset($_POST['nom'])) echo $_POST['nom']; ?> />
			<div class="clr"></div>
		</div>
		<div>
			<label <?php if(isset($emailIncomplet)) echo 'class="alert"'; ?> for="email">Email</label>
			<input type="text" name="email" id="email" size="30" maxlength="100" <?php if(isset($_POST['email'])) echo $_POST['email']; ?>/>
			<div class="clr"></div>
		</div>
		<div>
			<label <?php if(isset($mdpIncomplet)) echo 'class="alert"'; ?> for="mdp">Mot de passe</label>
			<input type="text" name="mdp" id="mdp" />
			<div class="clr"></div>
		</div>
		<div>
			<label <?php if(isset($confirmIncomplet)) echo 'class="alert"'; ?> for="confirm_mdp">Confirmation MDP</label>
			<input type="text" name="confirm_mdp" id="confirm_mdp" />
			<div class="clr"></div>
		</div>
	</fieldset>

	<fieldset class="forms">
		<legend>Informations personnelles</legend>
		
		<div>
			<div class="clr"></div>
		</div>
		
	</fieldset>

	<div class="submitDiv">
		<?php if(isset($_SERVER['HTTP_REFERER'])){ ?>
			<input type="hidden" name="HTTP_REFERER" value="<?php echo $_SERVER['HTTP_REFERER']; ?>" />
		<?php } ?>
		<input type="submit" value="S'inscrire" />
	</div>
	
</form>

<?php
include 'includes/bas.php';
?>