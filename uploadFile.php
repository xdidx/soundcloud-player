<?php
if(isset($_POST['keyFile'])) {

	header('Content-type:text/plain;charset=utf-8');
	
	$status = apc_fetch('upload_'.$_POST['keyFile']);
	exit;

}else{

	include 'includes/init.php';
	
	if(isset($_POST['nomMusique']) AND isset($_POST['artiste']) AND is_numeric($_POST['artiste'])){
		if(trim($_POST['nomMusique'])!=''){
			if(isset($_FILES['musicUploadingFile']) AND !empty($_FILES['musicUploadingFile']['name'])){

				$extensions_valides = array('mp3' , 'wav' , 'ogg'); //Liste des extensions valides
				
				if ($_FILES['musicUploadingFile']['error'] == 0){
					$extension_upload = pathinfo($_FILES['musicUploadingFile']['name'], PATHINFO_EXTENSION);
					if (in_array($extension_upload,$extensions_valides) AND !is_numeric(strpos($_FILES['musicUploadingFile']['name'],'.php'))){
					
						
						$prepare_artiste_upload = $bdd->prepare('SELECT * FROM artistes WHERE artistes_id=?') or die(mysql_error());
						$prepare_artiste_upload->execute(array($_POST['artiste']));
						if($fetch_artistes_upload = $prepare_artiste_upload->fetch()){
						
							$artisteId = $_POST['artiste'];
							$musicName = $_POST['nomMusique'];
						
							$prepareNewMusic = $bdd->prepare('INSERT INTO musiques(musiques_nom, musiques_artiste) values(?,?)') or die(mysql_error());
							$prepareNewMusic->execute(array($musicName, $artisteId));				
							$newMusicId=$bdd->lastInsertId();
							
							$chemin='musics/'.$artisteId.'/'.$newMusicId.'.'.$extension_upload;				
							if(move_uploaded_file($_FILES['musicUploadingFile']['tmp_name'],$chemin)){
								$state = $newMusicId;
							}else{
								$state = 'Le transfert a echou�. Veuillez r�essayer ult�rieurement.';
								$prepareNewMusic = $bdd->prepare('DELETE FROM musiques WHERE musiques_id=?') or die(mysql_error());
								$prepareNewMusic->execute(array($newMusicId));
							}
							
						}else
							$state = 'L\'artiste n\'est pas valide';
						
					}else
						$state = 'L\'extension du fichier n\'est pas valide. (Extensions support�es : MP3, WAV et OGG)';
					
				}else
					if($_FILES['musicUploadingFile']['error']==1)
						$state = 'La taille maximum par fichier pour l\\\'upload est de 50Mo';
					else
						$state = 'Il y a eu une erreur. (N�'.$_FILES['musicUploadingFile']['error'].')';
				
			}else{
				$state = 'Veuillez selectionner un fichier.';
			}
		}else{
			$state = 'Nom de la musique invalide.';	
		}
	}else{
		$state = 'Donn�es envoy�es �rronn�es.';
		
		if(!isset($_POST['nomMusique']) && !isset($_POST['artiste']))
			$state = 'Pas de post !!! ';
		else if(!is_numeric($_POST['artiste']))
			$state = 'POST artiste n\\\'est pas un chiffre : '.$_POST['artiste'];
	}
	?>
	<script language="javascript" type="text/javascript">
		window.top.window.uploadCallback('<?php echo $state; ?>');
	</script>
	<?php

}
?>