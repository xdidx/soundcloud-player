<?php
include 'includes/haut.php';
?>

<form action="uploadFile.php" method="post" enctype="multipart/form-data" target="upload_frame">

	<fieldset class="forms">
		
		<legend>Upload d'une musique</legend>
		
		<div id="uploadState">		
		
			<div>
				<label for="nomMusique">Nom de la musique :</label>
				<input type="text" name="nomMusique" id="nomMusique" />
				<div class="clr"></div>
			</div>
			<div>
				<label for="artiste">Artiste :</label>
				<select name="artiste" id="artiste">
					<?php
					$query_artistes_upload = $bdd->query('SELECT * FROM artistes') or die(mysql_error());
					for($nb_artistes=0;$fetch_artistes_upload = $query_artistes_upload->fetch();$nb_artistes++)
						echo '<option value="'.$fetch_artistes_upload['artistes_id'].'">'.$fetch_artistes_upload['artistes_nom'].'</option>';
					
					if($nb_artistes==0)
						echo '<option value="0">Aucun artiste disponible</option>';
					?>
				</select>
				<div class="clr"></div>
			</div>
			<div>
				<label for="musicUploadingFile">Fichier de musique :</label>
				<input type="hidden" name="APC_UPLOAD_PROGRESS" id="progress_key" value="<?php echo uniqid(); ?>" />
				<input type="file" name="musicUploadingFile" /><br/>
				<div class="clr"></div>
			</div>
		</div>
		
		<img id="uploadStateImg" alt="Etat de l'upload"/>

	</fieldset>
	
	<div class="submitDiv">
		<input type="submit" value="Valider" />
	</div>
		
</form>
		
<iframe style="display: none" id="upload_frame" name="upload_frame"></iframe>
	
<?php
include 'includes/bas.php';
?>