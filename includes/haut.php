<?php
include_once 'init.php';
?>
<!DOCTYPE html>
<html>

	<head>
		<title>Audio Player by DiD</title>

		<link href="includes/style.css" rel="stylesheet" type="text/css">
		
		<script src="includes/jquery.js"></script>
		<script src="includes/javascript.js"></script>
		
	</head>
	
	<body>
		<div id="menuBar">
			<div class="subMenuL">
				<a href="index.php">
					<img id="logoImg" src="images/logo.png" title="Accueil" alt="Logo" />
				</a>
			</div>
			<div class="subMenuL" id="volumeDiv">
				<div id="volumeGestionDiv">
					<div id="speakerImg"></div>
					<div id="volumeBack"></div>
					<div id="volume"></div>
				</div>
				<div id="volumeImg"></div>
			</div>	
			<?php
			$user = is_loged();
			if($user){
				?>
				<div class="subMenuR">
					<a class="menuLink" href="upload.php">Upload</a>
				</div>	
				<div class="subMenuR">
					<?php echo $user['artistes_nom']; ?>
					<br/>
					<a href="?deco">Se deconnecter</a>
				</div>	
				<?php				
			}else{
				?>
				<div class="subMenuR">
					<a class="menuLink" href="connexion.php">Connexion</a>
				</div>
				<?php
			}
			?>
		</div>
		
		<div id="corps">