<?php
session_start();

try{
	$bdd = new PDO("mysql:host=localhost;dbname=player", 'root', '');
}catch(Exception $e){
	die('Erreur : '.$e->getMessage());
}

function date_fr($date) {
    $texte_en = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "January","February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $texte_fr = array("Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche", "Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre");
    return str_replace($texte_en, $texte_fr, $date);
}

function is_loged(){
	try{
		$bdd = new PDO("mysql:host=localhost;dbname=player", 'root', '');
	}catch(Exception $e)	{
		die('Erreur : '.$e->getMessage());
	}
	
	if(isset($_SESSION['login'])){
		$query_login = $bdd->prepare('SELECT * FROM artistes WHERE artistes_id=? ') or die(mysql_error());
		$query_login->execute(array($_SESSION['login']));
		if($fetch_login = $query_login->fetch())
			return $fetch_login;
	}
	
	return false;
}

if(isset($_GET['deco']) && isset($_SESSION['login']))
	unset($_SESSION['login']);

?>