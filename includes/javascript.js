var volumeModif=false;//determine si mousemove change le volume ou non
var mousePosMoveX=0;//Position de la souris
var musicsVolume=100;//Volume quand non mute
var mutedVolume=false;//...

$(function(){

	$(document)
	.ready(function(){
		$('.barGroup')
		.each(function(){
			$(this).children('.waveBar').css('background-image','url(\'images/waves/'+$(this).attr('id').split('_')[1]+'.png\')');
		});
	});

	$('.barGroup')
	.click(function(e){		
		var musicId = $(this).attr('id').split('_')[1];
		var player = document.querySelector('#audioPlayer_'+musicId);
		
		var poucentMusic = ((e.pageX - $(this).offset().left) / $(this).width());		
		player.currentTime = poucentMusic * player.duration;
		
		if(player.paused)
			$('#play_'+musicId).click();
		
	});
	
	$('.play')
	.click(function(){
		var musicId = $(this).attr('id').split('_')[1];
		var player = document.querySelector('#audioPlayer_'+musicId);
		if(player.paused){
			player.play();
			$(this).css('background-position','0px -30px');
		}else{
			player.pause();
			$(this).css('background-position','0px 0px');
		}
	});
	
	$('#speakerImg')
	.click(function(){
		mutedVolume = mutedVolume ? false : true;
		updateVolume();
	});
	
	$('#volume')
	.click(function(e){
		if(e.pageX == undefined)
			e.pageX=mousePosMoveX;
		var newVol = e.pageX - $('#volume').offset().left;
		musicsVolume = newVol;
		updateVolume();
	})
	.mousedown(function(){
		volumeModif=true;
	});
	
	$('#volumeBack')
	.click(function(e){
		mousePosMoveX=e.pageX
		$('#volume').click();
	})
	.mousedown(function(){
		$('#volume').mousedown();
	});
	
	$('#volumeGestionDiv')
	.mousemove(function(e){
		if(volumeModif){
			mousePosMoveX=e.pageX
			$('#volume').click();
		}
	})
	.mouseup(function(){
		volumeModif=false;
	});
	
	$('#volumeDiv')
	.mouseenter(function(){
		$('#volumeGestionDiv').stop().animate({'width':'120px'},300);	
	})
	.mouseleave(function(){
		$('#volumeGestionDiv').stop().animate({'width':'12px'},300);	
		$('#volumeGestionDiv').mouseup();
	});
	
	$('#uploadButton')
	.click(function(){
		location.href='upload.php';
	});
	
	
	$('#uploadSubmitButton').click(function() {
		$('#uploadState').html('Upload de la musique en cours');
		$('#uploadStateImg').attr('src','images/loading.gif');
		$('#uploadStateImg').css('display','block');
		$(this).parent().submit();
		majPourcentUploadMusic();
	});
	
});

function majPourcentUploadMusic(){
	$.post('uploadFile.php',{keyFile:$('#progress_key').val()},function(data){
		$('#uploadStatePourcent').html(data);
		setTimeout('majPourcentUploadMusic()', 100);
	});
}

function uploadCallback(result){
	if(!isNaN(result)){
		$('#uploadState').html('Conversion en cours.');
		$.post('convert.php',{file:result},function(data){
			if(data=='true'){
				$('#uploadState').html('Cr�ation du spectre sonore en cours.');
				$.post('waveCreate.php',{file:result},function(data){
					if(data=='true'){
						$('#uploadState').html('Votre musique a bien �t� upload�e !<br/><a href="musics.php?music='+result+'">Cliquez ici pour y acc�der</a>');
						location.href='musics.php?music='+result;
					}else{
						$('#uploadState').html('Probl�me avec la cr�ation du spectre sonore : '+data);
					}
					$('#uploadStateImg').css('display','none');
				});
			}else{
				$('#uploadState').html('Probl�me avec la conversion : '+data);
				$('#uploadStateImg').css('display','none');
			}
		});
	}else{
		$('#uploadState').html(result);
		$('#uploadStateImg').css('display','none');
	}
}

function updateVolume(){

	if(musicsVolume>100) musicsVolume=100;
	if(musicsVolume<0) musicsVolume=0;
	
	var currentVolume = musicsVolume;
	if(mutedVolume)
		currentVolume=0;
	
	$('#volume').css('width',currentVolume+'px');
	var speakerState=1;//volume a fond
	if(currentVolume==0)
		speakerState=4;//mute
	else if(currentVolume<50)
		speakerState=3;//mute
	else if(currentVolume<95)
		speakerState=2;//mute
	
	var pxStateY = (-50*speakerState);		
	$('#volumeImg').css('background-position','0px '+pxStateY+'px');
	
	$('audio')
	.each(function(){
		$(this).volume = currentVolume;
	});
	
}

function update(musicId){
	var player = document.querySelector('#audioPlayer_'+musicId);
	
	if(isNaN(player.currentTime))
		var time = 0;
	else
		var time = player.currentTime;
		
	if(isNaN(player.duration)){
		var duration = 0;		
		var durationFraction = 0;
	}else{
		var duration = player.duration;
		var durationFraction = (time / duration)*100;
	}
		
	if(player.buffered.length!=0)
		var bufferedEnd=player.buffered.end(0);
	else
		var bufferedEnd=0;
	
	var bufferedFraction = (bufferedEnd/duration)*100;

	var progressBar = document.querySelector('#progressBar_'+musicId);
	var bufferedBar = document.querySelector('#bufferedBar_'+musicId);
	
	progressBar.style.width = durationFraction + '%';
	bufferedBar.style.width = bufferedFraction + '%';
		
	var audioState = document.querySelector('#audioState_'+musicId);
	var timeMinutes = Math.floor(time/60);
	
	if(timeMinutes.toString().length<2)
		timeMinutes='0'+timeMinutes;
	var timeSecondes = Math.ceil(time%60);
	if(timeSecondes.toString().length<2)
		timeSecondes='0'+timeSecondes;
	var durationMinutes = Math.floor(duration/60);
	if(durationMinutes.toString().length<2)
		durationMinutes='0'+durationMinutes;
	var durationSecondes = Math.ceil(duration%60);
	if(durationSecondes.toString().length<2)
		durationSecondes='0'+durationSecondes;
	audioState.textContent = timeMinutes+':'+timeSecondes+' / '+durationMinutes+':'+durationSecondes;
	
}