<?php

	
		$filename = 'musics/1/1.wav';

		if(file_exists($filename)){
			
			/*PARAMETRES*/
			$width = 1000;
			$height = 50;
			$foreground = "#000000";
			$background = "#ffffff";
			$draw_flat = true;

			$img = false;
			
			/*GENERAL FUNCTIONS*/
			function findValues($byte1, $byte2){
				$byte1 = hexdec(bin2hex($byte1));         
				$byte2 = hexdec(bin2hex($byte2));                        
				return ($byte1 + ($byte2*256));
			}

			function html2rgb($input) {
				$input=($input[0]=="#")?substr($input, 1,6):substr($input, 0,6);
				return array(
					hexdec(substr($input, 0, 2)),
					hexdec(substr($input, 2, 2)),
					hexdec(substr($input, 4, 2))
				);
			}
			
			// generate foreground color
			list($r, $g, $b) = html2rgb($foreground);

			$handle = fopen($filename, "r");
			// wav file header retrieval
			$heading[] = fread($handle, 4);
			$heading[] = bin2hex(fread($handle, 4));
			$heading[] = fread($handle, 4);
			$heading[] = fread($handle, 4);
			$heading[] = bin2hex(fread($handle, 4));
			$heading[] = bin2hex(fread($handle, 2));
			$heading[] = bin2hex(fread($handle, 2));
			$heading[] = bin2hex(fread($handle, 4));
			$heading[] = bin2hex(fread($handle, 4));
			$heading[] = bin2hex(fread($handle, 2));
			$heading[] = bin2hex(fread($handle, 2));
			$heading[] = fread($handle, 4);
			$heading[] = bin2hex(fread($handle, 4));

			// wav bitrate 
			$peek = hexdec(substr($heading[10], 0, 2));
			$byte = $peek / 8;

			// checking whether a mono or stereo wav
			$channel = hexdec(substr($heading[6], 0, 2));
			
			$ratio = ($channel == 2 ? 40 : 80);

			// start putting together the initial canvas
			// $data_size = (size_of_file - header_bytes_read) / skipped_bytes + 1
			$data_size = floor((filesize($filename) - 44) / ($ratio + $byte) + 1);
			define("DETAIL", 1000);
			$data_point = 0;
			
			while(!feof($handle) && $data_point < $data_size){
				if ($data_point++ % DETAIL == 0) {
					$bytes = array();

					// get number of bytes depending on bitrate
					for ($i = 0; $i < $byte; $i++)
						$bytes[$i] = fgetc($handle);

					switch($byte){
						// get value for 8-bit wav
						case 1:
							$data = findValues($bytes[0], $bytes[1]);
						break;
						// get value for 16-bit wav
						case 2:
							if(ord($bytes[1]) & 128)
								$temp = 0;
							else
								$temp = 128;
							$temp = chr((ord($bytes[1]) & 127) + $temp);
							$data = floor(findValues($bytes[0], $temp) / 256);
						break;
					}

					// skip bytes for memory optimization
					fseek($handle, $ratio, SEEK_CUR);

					// draw this data point
					// relative value based on height of image being generated
					// data values can range between 0 and 255
					$v = (int) ($data / 255 * $height);

					// don't print flat values on the canvas if not necessary
					if (!($v / $height == 0.5 && !$draw_flat))
					// draw the line on the image using the $v value and centering it vertically on the canvas

					//$dcol = imagecolorallocatealpha($img, 0, 0, 0, 127);
					$dcol = imagecolorallocate($img, 0, 0, 0);

					imageline(
						$img,
						// x1
						(int) ($data_point / DETAIL),
						// y1: height of the image minus $v as a percentage of the height for the wave amplitude
						$height - $v,
						// x2
						(int) ($data_point / DETAIL),
						// y2: same as y1, but from the bottom of the image
						$height - ($height - $v),
						$dcol
					);

				} else {
					// skip this one due to lack of detail
					fseek($handle, $ratio + $byte, SEEK_CUR);
				}
			}
			
			fclose($handle);
		}
 ?>